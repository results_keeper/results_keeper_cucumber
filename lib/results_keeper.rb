require 'net/http'
require 'json'
require 'rest_client'
require_relative 'string'
require 'singleton'

class ResultsKeeper
  include Singleton

  attr_reader :revision_name, :revision_project

  DEFAULT_PORT = 80
  DEFAULT_HOST = 'results-keeper.herokuapp.com'

  def initialize
    @revision_name = ENV['REVISION_NAME'] || "Result for #{Time.now}"
    @revision_project = ENV['PROJECT_NAME'] || 'Default'
  end

  def send_revision
    data = { revision: @revision_name, project: project_name }
    @revision = send_json(data, 'revisions')
  end

  def send_revision_complete
    data = { revision: @revision_name, project: project_name, completed: 1 }
    send_json(data, 'revisions')
  end

  def send_test(scenario)
    @screenshot_path = save_screenshot(scenario) if ENV['SEND_SCREENSHOTS']
    scenario_steps = scenario.test_steps.map(&:name) unless ENV['NO_STEPS']
    scenario_error = scenario.exception.message if scenario.exception
    run_path = "cucumber #{scenario.location.file}:#{scenario.location.line}"
    data = { name: scenario.name,
     status: scenario.status,
     feature_name: scenario.feature.name,
     run_path: run_path,
     error: scenario_error,
     revision_id: @revision['revision_id'],
     steps: scenario_steps
    }
    @test = send_json(data, 'tests')
    send_screenshot(@screenshot_path) if @screenshot_path
  end

  def save_screenshot(scenario)
    if scenario.exception
      screenshot_name = "#{Time.now.to_i}_#{rand(1000..9999)}.png"
      @file_path = "tmp/screenshots/#{screenshot_name}"
      Capybara.page.save_screenshot(@file_path)
      @file_path
    end
  end

  def send_json(body, path)
    @host = ENV['REPORT_SERVER_HOST'] || DEFAULT_HOST
    @port = ENV['REPORT_SERVER_PORT'] || DEFAULT_PORT

    body['secret_key'] = ENV['RK_SECRET_KEY']
    @path = "/api/#{path}"
    @body = body.to_json

    request = Net::HTTP::Post.new(@path, initheader = {'Content-Type' =>'application/json'})
    request.body = @body
    response = Net::HTTP.new(@host, @port).start {|http| http.request(request) }
    puts "    Results Keeper: #{response.code} - #{response.message}".blue
    result_hash = JSON.parse(response.body) rescue console_message('There seems to be a problem. Are you authorized?')
    result_hash
  end

  def send_screenshot(screenshot_path)
    if ENV['SEND_SCREENSHOTS']
      t1 = Thread.new do
        params = { project: @revision['project_id'], revision: @revision['revision_id'], test: @test['id'] }
        RestClient.post("http://#{@host}:#{@port}/api/tests/upload_screenshot",
                        :name_of_file_param => File.new(screenshot_path), :body => params)
        File.delete(screenshot_path)
      end
      t1.join
    end
  end

  def project_name
    ENV['PROJECT_NAME']
  end

  def console_message(text)
    puts "    Results Keeper: #{text}".blue
  end
end

