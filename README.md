# Results Keeper #

This is our first client.
It gathers cucumber test results and sends them to [Results Keeper](https://results-keeper.herokuapp.com) in real-time.

**Installation**

* gem "results_keeper" # add to your Gemfile or use "gem install results_keeper"
*  bundle install
*  add the following code to your env.rb or any file in "features/support" folder (we'd suggest to create a new file like "results_keeper_hooks.rb" and put it in "features/support" folder):


```
#!ruby

if ENV['SEND_RK_RESULTS']
  require 'results_keeper'

  ResultsKeeper.instance.send_revision

  Before do |scenario|
    ResultsKeeper.instance.test_started(scenario)
  end

  After do |scenario|
    ResultsKeeper.instance.send_test(scenario)
  end

  at_exit do
    ResultsKeeper.instance.send_revision_complete
  end
end

```

* go to [Results Keeper](https://results-keeper.herokuapp.com) website and create an account.
* click on your name at the top of Results Keeper page and copy your secret key (or follow [this link](https://results-keeper.herokuapp.com/users/999)). 

After you have your gem installed, above mentioned code is added to 'features/support' and you have your **secret** key, it's time to run your tests and to see the power of Results Keeper, using the following command:

```
#!bash

RK_SECRET_KEY=XXXXXXX-XXXXXXXX-XXXX-XXXXXXXXX cucumber /features
```
* add your RK_SECRET_KEY to .bashrc file, and you won't need to write it each time when you run your tests (not mandatory)

```
#!bash

echo "export RK_SECRET_KEY=XXXXXXX-XXXXXXXX-XXXX-XXXXXXXXX" >> ~/.bashrc
```
(XXXXXXX-XXXXXXXX-XXXX-XXXXXXXXX is your secret key)

**Passing params**

If you just run cucumber command Results Keeper will create a Default project (or will use it if it was created already) and it will create a new revision with a generated name. But all of these you can pass as a parameter.


```
#!bash
PROJECT_NAME=test cucumber
```
Will create or use already created "test" project and create a new revision with an automatically generated name. 

```
#!bash
PROJECT_NAME=test REVISION_NAME=revision_001 cucumber
```
Will create or use already created "test" project and create a new revision with the provided name (if the revision already exists, we will use the existing one as well).

```
#!bash
SEND_SCREENSHOTS=true cucumber
```
Will be sending screenshots on failures.

```
#!bash
NO_STEPS=true cucumber
```
Wont be sending cucumber steps.